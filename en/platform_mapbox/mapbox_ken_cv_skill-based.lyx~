#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass moderncv
\begin_preamble
\moderncvstyle{banking}
%\moderncvstyle{classic}
%\moderncvstyle{banking}
%\moderncvstyle{fancy}
%\moderncvstyle{oldstyle}
\moderncvcolor{blue} 
\firstname{\LARGE{Ken}} \familyname{\LARGE{Esparta Ccorahua}}
%\title{Curriculum Vitae}                                                                                                      
%\address{Av. Dom Jose Tupinambá da Frota 2280}{62010-295 Sobral}                                          
\mobile{(+51)~950146380}
\email{kenesparta@protonmail.com}                                                                               
\extrainfo{Nationality: Peruvian}  
%\photo[65pt]{../foto.jpg}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "biolinum" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family sfdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing other 1.5
\use_hyperref false
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\footskip 1cm
\secnumdepth 1
\tocdepth 1
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\bullet 0 1 35 1
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
maketitle
\end_layout

\end_inset


\end_layout

\begin_layout Section
Personal Statement
\end_layout

\begin_layout Standard
Graduated in Computer Engineering.
 I want to learn as much as I can about new technologies related to the
 Cloud Computing.
 Python, Golang and JavaScript are my favorites programming languages.
\end_layout

\begin_layout Section
Skills
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cvitem{
\end_layout

\end_inset

Respect for the unknown, and a desire to explore it.
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Data structure and algorithms are part of the Computer Engineering syllabus.
\end_layout

\begin_layout Itemize
Some projects was done such as solving problems using lists, stacks and
 trees.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cvitem{
\end_layout

\end_inset

Knowledge of Python
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Good understanding of structured and object-oriented syntax.
\end_layout

\begin_layout Itemize
Little understanding about the functional paradigm syntax.
 However, I want to learn it.
\end_layout

\begin_layout Itemize
Knowledge of some design patterns and unit tests.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cvitem{
\end_layout

\end_inset

Knowledge of JavaScript
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Understanding the structured paradigm syntax.
\end_layout

\begin_layout Itemize
Little understanding JavaScript functional paradigm syntax.
\end_layout

\begin_layout Itemize
Basic knowledge about ECMAScript 2015 and ECMAScript 2016.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cvitem{
\end_layout

\end_inset

Shell Script comprehension
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Basic commands are well understood.
\end_layout

\begin_layout Itemize
Shell Script was used to automate the basic Virtual Private Server configuration.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cvitem{
\end_layout

\end_inset

Ability to read in English
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Good reading comprehension.
\end_layout

\begin_layout Itemize
Slight ability to write in English, but I want to improve.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Education
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cventry{2013--2016}{
\end_layout

\end_inset

Federal University of Ceará
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset

Computer Engineering
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset

Sobral
\begin_inset ERT
status open

\begin_layout Plain Layout

--
\end_layout

\end_inset

CE Brazil
\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}{
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Work
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cventry{Oct.
 2016 -- Present}{
\end_layout

\end_inset

Founder, Junior Developer and Junior Linux Administrator
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Ship Web Services
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Sobral
\begin_inset ERT
status open

\begin_layout Plain Layout

--
\end_layout

\end_inset

CE Brazil
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Virtual Private Server (VPS) configuration.
\end_layout

\begin_layout Itemize
Construction of a platform as a service (PaaS) using Docker technology.
\end_layout

\begin_layout Itemize
Website: 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
url{
\end_layout

\end_inset

shipwebservices.com
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cventry{Aug.
 -- Dec.
 2016}{
\end_layout

\end_inset

Junior Python Developer
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Ware Soluções Digitais
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Sobral
\begin_inset ERT
status open

\begin_layout Plain Layout

--
\end_layout

\end_inset

CE Brazil
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
API developed in Flask (Python Framework).
\end_layout

\begin_layout Itemize
Virtual Private Server configuration.
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
vspace{0.3cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cventry{Jul.
 2015 -- Dec.
 2016}{
\end_layout

\end_inset

Junior Web Developer
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Loading Jr.
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset

Sobral
\begin_inset ERT
status open

\begin_layout Plain Layout

--
\end_layout

\end_inset

CE Brazil
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout

{
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Websites development using JavaScript and HTML.
\end_layout

\begin_layout Itemize
Website: 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
url{
\end_layout

\end_inset

loadingjr.com.br
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

}
\end_layout

\end_inset


\end_layout

\end_body
\end_document
